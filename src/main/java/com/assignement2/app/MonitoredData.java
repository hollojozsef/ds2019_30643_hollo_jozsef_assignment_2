package com.assignement2.app;

import java.time.Duration;
import java.time.LocalDateTime;
//import java.time.format.DateTimeFormatter;
//
//import org.joda.time.DateTime;
//import org.joda.time.Interval;

public class MonitoredData {
    public LocalDateTime startTime;
    public LocalDateTime endTime;
    public String activity;
    public MonitoredData(LocalDateTime startTime, LocalDateTime endTime, String activity) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity = activity;
    }
    public LocalDateTime getStartTime() {
        return startTime;
    }
    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }
    public LocalDateTime getEndTime() {
        return endTime;
    }
    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }
    public String getActivity() {
        return activity;
    }
    public void setActivity(String activity) {
        this.activity = activity;
    }
    public Duration getDuration() {
        LocalDateTime tempDateTime = LocalDateTime.from( startTime );
        Duration dur = Duration.between(startTime, endTime);

        //Interval period = new Interval(getStartTime(), getEndTime());
        return dur;
    }
    @Override
    public String toString() {
        return "startTime=" + startTime + ", endTime=" + endTime + ", activity=" + activity ;
    }

}
