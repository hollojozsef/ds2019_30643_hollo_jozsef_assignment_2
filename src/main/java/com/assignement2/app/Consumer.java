package com.assignement2.app;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.CommandLineRunner;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
public class Consumer implements CommandLineRunner {


    private final RabbitTemplate rabbitTemplate;
    private final Producer producer;
    ArrayList<MonitoredData> data = new ArrayList<MonitoredData>();
    File file=new File("activity.txt");
    private int index=1;
    public Consumer(Producer producer, RabbitTemplate rabbitTemplate) {
        this.producer = producer;
        this.rabbitTemplate = rabbitTemplate;
    }

    @Override
    public void run(String... args) throws Exception {

        read(file);
//        getActivity();
        checkActivity();
        System.out.println("Sending message...");
        rabbitTemplate.convertAndSend(AppApplication.topicExchangeName, "foo.bar.baz", data.get(index).toString());
        producer.getLatch().await(10000, TimeUnit.MILLISECONDS);
        index++;
    }


    public void read(File file) throws IOException {
        Scanner scanner = new Scanner(file);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        while (scanner.hasNext()) {
            String[] tokens = scanner.nextLine().split("		");
           // LocalDateTime start = formatter.parse(tokens[0]);
            LocalDateTime start = LocalDateTime.parse(tokens[0],formatter);
            LocalDateTime end = LocalDateTime.parse(tokens[1],formatter);
           // LocalDateTime end = formatter.parse(tokens[1]);
            String[] tokens2 = tokens[2].split("	");
            String activity = tokens2[0];
            MonitoredData mon = new MonitoredData(start, end, activity);
            //System.out.println(mon);

            data.add(mon);
        }
    }
    public void getActivity() {

        Map<String, Long> act = data.stream()
                .collect(Collectors.groupingBy(data -> data.getActivity(), Collectors.counting()));

            PrintWriter out;
        try {
            out = new PrintWriter(new FileWriter("AllActivities.txt", true), true);
           out.println(act);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public void checkActivity(){
        for (MonitoredData temp:data){
            //System.out.println("Activity "+temp.toString() + "is longer than 12 hours!     "+temp.getDuration().toHours());
             if(temp.getDuration().toHours()>12 && temp.getActivity().equals("Sleeping"))
            System.out.println("Activity "+temp.toString() + " is longer than 12 hours!");
            if(temp.getDuration().toHours()>12 && temp.getActivity().equals("Leaving"))
                System.out.println("Activity "+temp.toString() + " is longer than 12 hours!");
            if(temp.getDuration().toHours()>1 && temp.getActivity().equals("Toileting"))
                System.out.println("Activity "+temp.toString() + " is longer than 1 hours!");
        }
    }
}
